# ruff: noqa: T201 (print)

# Untyped decorator bot.message_handler
# mypy: disable-error-code="misc"


import itertools
import json
import logging as log
import os
import random
import typing
from pathlib import Path

import telebot  # type: ignore[import-untyped]
import urllib3

TELEGRAM_TOKEN = os.environ['TELEGRAM_TOKEN']
TPG_TOKEN = os.environ['TPG_TOKEN']

TELEGRAM_CHAT_ID = -1001593071239

drinkingbuddy_url = 'http://drinkingbuddy.lan.posttenebraslab.ch/beverages'
drinkingbuddy_category_soft = 1
drinkingbuddy_category_beer = 2
drinkingbuddy_category_energy = 4

reply_keyboard_remove = telebot.types.ReplyKeyboardRemove()


# only used for console output now
def listener(messages: list[telebot.types.Message]) -> None:
    """When new messages arrive TeleBot will call this function.

    :param messages: message
    """
    for message in messages:
        if message.content_type == 'text':
            log.info('Received command `%s`', message.text)


def get_json(url: str) -> typing.Any:  # noqa: ANN401 (any)
    """Get the full status from the status api.

    Return the Json parsed

    :param url: url to retrieve data from
    :return: json data
    """
    http = urllib3.PoolManager()
    response = http.request('GET', url)
    return json.loads(response.data.decode('utf-8'))


log.basicConfig(level=log.WARNING)

bot = telebot.TeleBot(TELEGRAM_TOKEN)
bot.set_update_listener(listener)  # register listener

T = typing.TypeVar('T')


def shuffled(*elements: T) -> list[T]:
    return random.sample(elements, k=len(elements))


ADS = iter(
    zip(
        itertools.cycle(
            shuffled(
                lambda sponsor: f'{sponsor} !',
                lambda sponsor: f'But first, a quick message from our sponsor {sponsor} !',
                lambda sponsor: f'PTL is brought to you by {sponsor} !',
                lambda sponsor: f'{sponsor} ? Yes ! {sponsor} !',
                lambda sponsor: f'And here is a message from {sponsor} !',
                lambda sponsor: f'Who the hell is {sponsor} ?',
            ),
        ),
        itertools.cycle(
            itertools.chain(
                (  # always start with nord VPN
                    ['NordVPN', 'protects you from anything, such as covid and aids', 'super duper cheap'],
                ),
                shuffled(  # guarantee each batch start with less crazy messages
                    ['Kiwiko', 'teaching you how to make artisanal bombs', 'enabling you to cook meth'],
                    ['Honey', 'not related to insects at all', 'like an ad thing, probably'],
                    [
                        'Brilliant',
                        'teaching you how to read if you are an audible client',
                        'making anything fun and easy',
                    ],
                    [
                        'Hello fresh',
                        'making you live the PTL Tuesday night adventure, but at home on your own',
                        'efficient at waste management',
                    ],
                ),
                shuffled(
                    ['Audible', 'making you read while sleeping', 'now free for a month with the promo-code PTL'],
                    [
                        'Morning brew',
                        'telling you stuff that have nothing to do with your life',
                        'bringing all sort of bad news',
                    ],
                    ['Shadow legends™', 'wasting a lot of your time', 'making a ton of money out of a few of you'],
                    ['Corn hub', 'producing high quality family entertainment', 'not really safe for work'],
                    ['FTX', 'your fastest grown scam', 'honest and totally not in jail right now'],
                    ['PTL lunch', 'feeding hackers every Tuesday', 'on permanent promotion at 4.-CHF'],
                    ['paperclips', 'paperclips', 'paperclips'],
                    ['undefined', 'NullPointerException', 'segmentation fault'],
                ),
            ),
        ),
    ),
)


@bot.message_handler(commands=['ads'])
def command_ads(request: telebot.types.Message) -> None:
    accroche, (sponsor, desc1, desc2) = next(ADS)
    desc1, desc2 = shuffled(desc1, desc2)
    response_text = '\n'.join(
        (
            typing.cast(typing.Callable[[str], str], accroche)(sponsor),
            f"{sponsor} is {desc1}. Moreover it's {desc2} !",
        ),
    )
    bot.send_message(request.chat.id, response_text)


# help page
@bot.message_handler(commands=['help'])
def command_help(request: telebot.types.Message) -> None:
    help_text = """The following commands are available:
/help: Gives you information about the available commands
/status: Get the Post Tenebras Lab presence status
/beer: How many beer left in fridge
/soft: How many soft drink left in fridge
/energy: How many energy drink left in fridge
/tpg: /tgp <line nb (12|15|11) return nexts departures>
/food: /food [question] Create a poll to ask who's joining for food
/ads: useful message from our sponsor
"""

    log.info('%s request help', request.chat.id)
    bot.send_message(request.chat.id, help_text)


@bot.message_handler(commands=['status'])
def command_status(request: telebot.types.Message) -> None:
    """Return the space API status."""
    response = get_json('https://www.posttenebraslab.ch/status/json/')
    state = response['state']

    if state['open']:
        response_text = f""" \N{HEAVY CHECK MARK} {state['message']}
Call {response['contact']['phone']} to be let in"""
    else:
        response_text = f""" \N{HEAVY MULTIPLICATION X} {state['message']}

  {response['contact']['email']}

  {response['url']}"""

    bot.send_message(request.chat.id, response_text)


def drinkingbuddy_entries(category: int) -> str:
    entries = get_json(drinkingbuddy_url)
    for entry in entries:
        if entry['category_id'] != category:
            continue
        quantity = entry['quantity']
        if quantity <= 0:
            continue
        rating = 'high' if quantity >= entry['minquantity'] else 'low'
        yield f'  - {quantity} {entry['name']} ({rating})'


@bot.message_handler(commands=['beer'])
def command_beer(request: telebot.types.Message) -> None:
    """Return the number of beer in fridge now."""
    entries = drinkingbuddy_entries(drinkingbuddy_category_beer)
    entries_list = '\n'.join(entries)

    if not entries_list:
        with Path('skull_beer.jpg').open('rb') as photo:
            bot.send_photo(request.chat.id, photo, reply_markup=reply_keyboard_remove)
        return

    response_text = f"""You're a lucky hacker there is some fresh beer in the lab :
{entries_list}
"""
    bot.send_message(request.chat.id, response_text)


@bot.message_handler(commands=['soft'])
def command_soft(request: telebot.types.Message) -> None:
    """Return the number of soft drink in fridge now."""
    entries = drinkingbuddy_entries(drinkingbuddy_category_soft)
    entries_list = '\n'.join(entries)

    if entries_list:
        response_text = f"""You're a lucky hacker there is some fresh beverages in the lab :
{entries_list}
"""
    else:
        response_text = '\N{SKULL}'

    bot.send_message(request.chat.id, response_text)


@bot.message_handler(commands=['energy'])
def command_energy(request: telebot.types.Message) -> None:
    """Return the number of energy drink in fridge now."""
    entries = drinkingbuddy_entries(drinkingbuddy_category_energy)
    entries_list = '\n'.join(entries)

    if entries_list:
        response_text = f"""You're a lucky hacker there is some fresh beverages in the lab :
{entries_list}
"""
    else:
        response_text = '\N{SKULL}'

    bot.send_message(request.chat.id, response_text)


bus_stop_names = {
    'TCAR': 'Tours-de-Carouge',
    'ARME': 'Armes',
    'INDT': 'Industrielle',
}
bus_line_stops = {
    '21': 'TCAR',
    '12': 'ARME',
    '15': 'INDT',
    '18': 'ARME',
}
bus_lines = ' '.join(bus_line_stops.keys())


@bot.message_handler(commands=['tpg'])
def command_tpg(request: telebot.types.Message) -> None:
    """Return the nexts bus/tram departures from closest bus stop.

    see : http://www.tpg.ch/web/open-data/mode-d-emploi
    """

    def get_bus_stop() -> tuple[str | None, str | None]:
        text_split = request.text.split()
        if not len(text_split) > 1:
            return (None, None)
        bus_line = text_split[1]
        return (bus_line, bus_line_stops.get(bus_line))

    bus_line, bus_stop_code = get_bus_stop()
    if bus_stop_code is None:
        bot.send_message(request.chat.id, f'Usage : /tpg 12\nCould be line number {bus_lines}')
        return

    url = f'http://prod.ivtr-od.tpg.ch/v1/GetNextDepartures?key={TPG_TOKEN}&stopCode={bus_stop_code}'
    tpg_response = get_json(url)
    response_text = f'Next departure line {bus_line} {bus_stop_names[bus_stop_code]}:\n'
    for departure in tpg_response['departures']:
        departure_line = departure['line']
        if bus_line == departure_line['lineCode']:
            response_text += f' - destination {departure_line['destinationName']} : {departure['waitingTime']}min \n'
    bot.send_message(request.chat.id, response_text)


@bot.message_handler(commands=['food'])
def command_food(request: telebot.types.Message) -> None:
    """Create a poll for food (mainly) for Tuesday's nights."""
    request_tokens = request.text.split(maxsplit=1)
    poll_question = request_tokens[1] if len(request_tokens) > 1 else 'Food tonight'
    poll_question += """
Matrix users: vote by reacting \N{MEAT ON BONE} or \N{AUBERGINE} to this message."""

    bot.send_poll(
        request.chat.id,
        question=poll_question,
        is_anonymous=False,
        options=[
            '\N{MEAT ON BONE} Omnivore',
            '\N{AUBERGINE} Vege',
            "Present but don't eat",
            'Not coming',
        ],
    )


# default handler for every other text
@bot.message_handler(func=lambda _: True, content_types=['text'])
def command_default(request: telebot.types.Message) -> None:
    if request.chat.id == TELEGRAM_CHAT_ID:
        # don't respond in the group chat
        return
    bot.send_message(request.chat.id, f'I don\'t understand "{request.text}"\nMaybe try the help page at /help')


print('Launch bot...')
log.info('bot started...')
bot.infinity_polling()
