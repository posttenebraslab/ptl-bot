# ptl-bot

The PTL bot, snitching on the lab's whereabouts.

## Setup

- Create a venv

    `python -m venv --upgrade-deps .venv`

- Install Python package dependencies

    `.venv/bin/pip install --constraint requirements-dev.txt --editable '.[dev]'`

## Build

This build is also run by GitLab CI on every push.

- Create container image

    `podman build --tag registry.gitlab.com/posttenebraslab/ptl-bot:latest .`

- Push container image to registry

    `podman push registry.gitlab.com/posttenebraslab/ptl-bot:latest`

## Run locally

- Copy `.env.example` to `.env` and fill in the passwords

- Start the bot

    `.venv/bin/dotenv run -- .venv/bin/python -m ptl_bot`

## CI/CD

GitLab CI builds a new docker image on every push: [.gitlab-ci.yml](.gitlab-ci.yml)

## Requirements update

    `.venv/bin/pip-compile --output-file=requirements.txt --strip-extras && .venv/bin/pip-compile --extra=dev --output-file=requirements-dev.txt --strip-extras`
